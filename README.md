

------

## 环境：

[jkd8+]()

[mysql5.6+]()



## 一、原理

当服务任务执行完后，边界事件（边界补偿事件）触发，本例定义了多实例服务任务，触发多次边界补偿事件。边界补偿事件会注册到流程中，每条执行实例对应注册表act_ru_event_subscr中生成对应的每条记录。

当补偿中间事件触发时，流程会转向补偿边界事件并触发补偿，补偿的执行次数与对应的流程活动的执行次数相等，所以引发多次补偿。





## 二、流程图

1、服务任务上定义补偿边界事件(补偿事件)，此处是多实例节点。

2、补偿边界事件指定具体补偿处理器：比如服务节点、任务节点等等。

3、定义了一个抛出BpmnError的服务节点，在此节点上定义了异常边界事件

4、异常边界事件触发时，指定了一个补偿中间事件，此补偿中间事件指定了补偿范围：补偿事件所在的服务任务节点。



![](./images/process.png)

## 三、补偿处理器及补偿中间事件定义

- 补偿处理器需要只当isForCompensation="true" ，[详见官网]，(https://www.flowable.org/docs/userguide/index.html#bpmnBoundaryCompensationEvent)
flowable6.4.0代码有bug,流程设计器上勾选了，但是导出xml时候，丢失了isForCompensation属性。后续有空修改此bug

- 补偿中间事件定义，[参考官网](https://www.flowable.org/docs/userguide/index.html#bpmnIntermediateThrowCompensationEvent)，因为flowable6.4.0没有补偿中间事件的图标。所以先用其中间无抛出事件，然后修改xml。

   参数`activityRef`可用于触发特定范围或活动的补偿

  ```
  <intermediateThrowEvent id="throwCompensation" name="补偿中间事件">
  	<compensateEventDefinition activityRef="servicetask" />
  </intermediateThrowEvent>
  ```


## 四、实践测试



- 运行demo
- 查看数据库表

