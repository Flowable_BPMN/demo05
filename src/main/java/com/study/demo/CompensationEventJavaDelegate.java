package com.study.demo;

import java.util.Map;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 服务任务--补偿边界事件
 * 
 *
 */
public class CompensationEventJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		System.out.println("========================开始执行服务任务,服务任务--补偿边界事件========================");
		//获取当前节点
		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点名称: " + flowElement.getName());
		
		//获取所有变量
		Map<String, Object>  vars = execution.getVariables();
		for(Map.Entry<String, Object> entry: vars.entrySet()) {
			System.out.println("key: " + entry.getKey() + ",value: "+ entry.getValue());
		}
		System.out.println("========================服务任务执行完毕,服务任务--补偿边界事件========================");
	}

}
