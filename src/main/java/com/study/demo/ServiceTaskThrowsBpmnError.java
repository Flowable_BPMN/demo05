package com.study.demo;

import org.flowable.engine.delegate.BpmnError;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class ServiceTaskThrowsBpmnError implements JavaDelegate{

	public void execute(DelegateExecution execution) {

       System.out.println("================抛出异常BpmnError===============");
		throw new BpmnError("code","抛出BpmnError后,补偿程序做出补偿处理");
	}

}
